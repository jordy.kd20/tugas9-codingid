﻿using BackendBootcamp.Logics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.Data;
using System.Security.Claims;
using tugas8_codingid.Logics;
using BackendBootcamp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace tugas8_codingid.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        [HttpGet]
        [Route("GenerateToken")]
        public ActionResult GenerateToken() 
        {
            string tokenJwt = JwtTokenLogic.GenerateJwtToken(new[]
            {
                new Claim("name", "Coding-Id" ?? ""),
                new Claim(ClaimTypes.Role, "admin"?? "")
            });

            return Ok(tokenJwt);
        }

        [HttpGet]
        [Route("ValidateToken")]
        public ActionResult ValidateToken([FromHeader] string token)
        {
            if (token == null)
            {
                return StatusCode(401, "401 Unauthorize");
            }
            try
            {
                var isAuth = JwtTokenLogic.ValidateJwtToken(token);
                if (isAuth == null)
                {
                    return StatusCode(401, "401 Unauthorize");
                }
                string role = isAuth.First(x => x.Type == "role").Value;
                if (role != "admin" && role != "user")
                {
                    return StatusCode(401, "Your roles is not high enough to access this!");
                }
                return StatusCode(200, "200 Success");
                return Ok("Congrats, your token is right!!");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("ValidateTokenwithAutorize")]
        [Authorize]
        public ActionResult ValidateTokenwithauto([FromQuery] string? name)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string currentUser = Convert.ToString(identity.FindFirst("name").Value);

                //return Ok(currentUser);
                return StatusCode(200, "200 Success");
                return Ok("Congrats, your token is right!!");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }

    
}
